import java.util.*

class Taulells(val preuUnitari: Double,val llargada:Double, val amplada:Double){
    fun precio(): Double{
        return llargada*amplada*preuUnitari
    }
}
class Llisto(val preuUnitari: Double, val llargada: Double){
    fun precio(): Double{
        return llargada*preuUnitari
    }
}
fun main(){
    val scanner = Scanner(System.`in`)
    var precioTotal = 0.0
    var numEntradas = scanner.nextInt()
    while(numEntradas != 0){
        val tipo = scanner.next().lowercase()
        if(tipo == "taulell"){
            val preuUnitari = scanner.nextDouble()
            val llargada = scanner.nextDouble()
            val amplada = scanner.nextDouble()
            precioTotal += Taulells(preuUnitari, llargada, amplada).precio()
        }else{
            val preuUnitari = scanner.nextDouble()
            val llargada = scanner.nextDouble()
            precioTotal += Llisto(preuUnitari, llargada).precio()
        }
        numEntradas--
    }
    println("El preu total és: $precioTotal€")
}