import java.util.Scanner

class BraçRobotic(){
    fun toggle(){
        encendido = !encendido
        println("MechanicalArm{openAngle=${obertura.toDouble()}, altitude=${altura.toDouble()}, turnedOn=$encendido}")
    }
    fun updateAltitude(entrada: Int){

        if(altura+entrada < 0){
            altura = 0
        }else if(altura+entrada > 30){
            altura = 30
        } else{
            altura+=entrada
        }
        println("MechanicalArm{openAngle=${obertura.toDouble()}, altitude=${altura.toDouble()}, turnedOn=$encendido}")
    }
    fun updateAngle(entrada: Int){
        if(obertura+entrada > 360){
            obertura = (obertura+entrada)-360
        }else{
            obertura+=entrada
        }
        println("MechanicalArm{openAngle=${obertura.toDouble()}, altitude=${altura.toDouble()}, turnedOn=$encendido}")
    }
}

var encendido = false
var obertura = 0
var altura = 0

fun main(){
    BraçRobotic().toggle()
    BraçRobotic().updateAltitude(3)
    BraçRobotic().updateAngle(180)
    BraçRobotic().updateAltitude(-3)
    BraçRobotic().updateAngle(-180)
    BraçRobotic().updateAltitude(3)
    BraçRobotic().toggle()
}