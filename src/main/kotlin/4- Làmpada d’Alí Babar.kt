import java.util.Scanner

class Lampada(val lampada: String){
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED,ANSI_GREEN, ANSI_YELLOW, ANSI_PURPLE, ANSI_CYAN)
    fun turnOn(){
        if(lampada=="1"){
            intensitat=1
            color=colorGuardado
            off=false
        }else{
            intensitat2=1
            color2=colorGuardado
            off2=false
        }
    }
    fun turnOff(){
        if (lampada=="1"){
            colorGuardado = color
            color = 0
            intensitat=0
            off = true
        }else{
            colorGuardado2 = color
            color2 = 0
            intensitat2=0
            off2 = true
        }
    }
    fun changeColor(){
        if(lampada=="1"){
            if(!off){
                if (color==colors.lastIndex) color=1
                else color++
            }
        }else{
            if(!off2){
                if (color2==colors.lastIndex) color2=1
                else color2++
            }
        }
    }
    fun intensity(){
        if(lampada=="1"){
            if(!off){
                if(intensitat < 5){
                    intensitat++
                }
            }
        }else{
            if(!off2){
                if(intensitat2 < 5){
                    intensitat2++
                }
            }
        }
    }
}

var color=0
var colorGuardado = 0
var intensitat=0
var off = true
const val ANSI_RESET = "\u001B[0m"
const val ANSI_BLACK = "\u001B[40m"
const val ANSI_RED = "\u001B[41m"
const val ANSI_GREEN = "\u001B[42m"
const val ANSI_YELLOW = "\u001B[43m"
const val ANSI_BLUE = "\u001B[34m"
const val ANSI_PURPLE = "\u001B[45m"
const val ANSI_CYAN = "\u001B[46m"
const val ANSI_WHITE = "\u001B[47m"

var color2=0
var colorGuardado2 = 0
var intensitat2=0
var off2 = true

fun main(){
    val scanner = Scanner(System.`in`)
    val colors = arrayOf(ANSI_BLACK, ANSI_WHITE, ANSI_RED,ANSI_GREEN, ANSI_YELLOW, ANSI_PURPLE, ANSI_CYAN)

    while(true){
        //println("Introdueix la lampada que vols canviar: ")
        //val lampada = scanner.next().lowercase()
        print("Introdueix ordre: ")
        val instruc= scanner.nextLine().uppercase()
        print("Quina lampada vols modificar:\n" +
                "1- Menjador\n" +
                "2- Habitació\n")
        val lampada = scanner.nextLine()
        var sortida = ""
        if(lampada == "1") sortida = "Menjador"
        else sortida = "Habitació"
        when(instruc) {
            "EXIT" ->{
                break
            }
            "TURN ON" -> {
                Lampada(lampada).turnOn()
            }
            "TURN OFF" -> {
                Lampada(lampada).turnOff()
            }
            "CHANGE COLOR" -> {
                Lampada(lampada).changeColor()
            }
            "INTENSITY" -> {
                Lampada(lampada).intensity()
            }
        }
        if(lampada=="1"){
            println("\nLampada: $sortida Color: ${colors[color]}  $ANSI_RESET  $ANSI_BLUE - intensitat $intensitat $ANSI_RESET")
        }else{
            println("\nLampada: $sortida Color: ${colors[color2]}  $ANSI_RESET  $ANSI_BLUE - intensitat $intensitat2 $ANSI_RESET")
        }
    }
}