class Pasta1(val nom:String, val preu:Double, val pes:Double, val calories:Int){

    fun printAll():String{
        return "Nombre: $nom\nPrecio: $preu\nPeso: $pes\nCalorias: $calories"

    }
}

fun main(){
    val croissant = Pasta1("croissant", 1.50,200.0, 150)
    val ensaimada = Pasta1("ensaimada", 1.75,300.0, 200)
    val donut = Pasta1("donut", 1.25,150.0, 125)

    println(croissant.printAll())
    println()
    println(ensaimada.printAll())
    println()
    println(donut.printAll())
}