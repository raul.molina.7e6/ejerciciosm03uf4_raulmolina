class Pasta2(val nom:String, val preu:Double, val pes:Double, val calories:Int){

    fun printAll():String{
        return "Nombre: $nom\nPrecio: $preu\nPeso: $pes\nCalorias: $calories"

    }
}
class Beguda(val nom:String, var preu:Double, val ensucrada:Boolean){
    init {
        if(ensucrada){
            preu += ((preu*10)/100)
        }
    }
    fun printAll():String{
        return "Nombre: $nom\nPrecio: $preu\nEs ensucrada: $ensucrada"
    }
}

fun main(){
    val croissant = Pasta2("croissant", 1.50,200.0, 150)
    val ensaimada = Pasta2("ensaimada", 1.75,300.0, 200)
    val donut = Pasta2("donut", 1.25,150.0, 125)

    val agua = Beguda("Aigua", 1.00, false)
    val cafe = Beguda("Café tallat", 1.35, false)
    val te = Beguda("Té vermell", 1.50, false)
    val cola = Beguda("Cola", 1.65, true)

    println(croissant.printAll())
    println()
    println(ensaimada.printAll())
    println()
    println(donut.printAll())
    println()
    println(agua.printAll())
    println()
    println(cafe.printAll())
    println()
    println(te.printAll())
    println()
    println(cola.printAll())
}